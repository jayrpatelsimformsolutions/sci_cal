var screenH:any = document.querySelector('#screen') as HTMLInputElement;
var btn:any = document.querySelectorAll('.btn');


// all eval related executions
for (let item of btn) {
    item.addEventListener('click', (e:any) => {
       const input = e.target as HTMLElement;
       let btntext:any = input.innerText;

        switch (btntext) {
            case '×':
                btntext = '*';
                break;
            case '÷':
                btntext = '/';
                break;
            case 'mod':
                break;   

        }
        screenH.value += btntext;
    });
}


// all on click related executions
function pow() {
    screenH.value = Math.pow(screenH.value, 2);
}

function regTodeg()
{
    screenH.value=screenH.value * (180/Math.PI);
}

function sqrt() {
    screenH.value = Math.sqrt(screenH.value);
}

function exp() {

    screenH.value = Math.exp(screenH.value);

}

function eva()
{
    screenH.value=eval(screenH.value)
}

function pi() {
    if (screenH.value == 0) {
        screenH.value = Math.PI;
    } else {
        screenH.value = screenH.value * Math.PI;
    }
}

function ln() {
    screenH.value = Math.log(screenH.value);
}

function fact() {
    var i, num, f;
    if (screenH.value == 0) {
        f = 0;
    } else {
        f = 1;
    }
    num = screenH.value;
    for (i = 1; i <= num; i++) {
        f = f * i;
    }
    i = i - 1;

    screenH.value = f;
}


function divide() {
    screenH.value = 1 / screenH.value;
}

function powx(){
    screenH.value = 10 ** screenH.value;
}

function negpos() {

    if (screenH.value == 0) {
        screenH.value = '-';
    } else {
        screenH.value = screenH.value * -1;
    }
}


function e() {
    if (screenH.value == 0) {
        screenH.value = Math.E;
    } else {
        screenH.value = screenH.value * Math.E;
    }
}

function sin(){
    screenH.value = Math.sin(screenH.value);
}

function cos() {
    screenH.value = Math.cos(screenH.value);
}

function tan() {
    screenH.value = Math.tan(screenH.value);
}

function log()
{
    screenH.value = Math.log10(screenH.value);
}

function sinI() {
     screenH.value = Math.asin(screenH.value);
}

function cosI() {
    screenH.value = Math.acos(screenH.value);
}

function tanI() {
    screenH.value = Math.atan(screenH.value);
}

function cot() { 
    screenH.value = 1 / Math.tan(screenH.value); 
}

function cotI() { 
    screenH.value = Math.PI / 2 - Math.atan(screenH.value);
 }

function abs()
{
    screenH.value=Math.abs(screenH.value);
}

function backspc() {
    screenH.value = screenH.value.slice(0, -1);
}

function c(){
    screenH.value='';
}

function r()
{
    screenH.value+='**';
}


/**
 * handle all type of memory opeartions
 *
 * @param   {string}  opration  take which opeartion to perform
 *
 * @return  {int}            return result of an expersion
 */
function memory(opration:any) {
    var memoryH:any = document.getElementById("memory");
    screenH.value = eval(screenH.value);
    if (isNaN(screenH.value) != false) {
        screenH.value = "Math Error";
    } else {
        switch (opration) {
            case '+':
                screenH.value = parseInt(memoryH.value) + parseInt(screenH.value);
                break;
            case '-':
                screenH.value = parseInt(memoryH.value) - parseInt(screenH.value);
                break;

        }
        memoryH.value = screenH.value;
    }

}

/**
 * convert normal number to expontaiol
 *
 * @return  {exponation}  returned exponation number
 */
function fe() {
    var num = parseInt(screenH.value);
    screenH.value = num.toExponential();

}

// for 2^nd button functionality
function change() {

    var state:any = document.getElementById('snd');

    // console.log( document.getElementsByClassName("ndd"));
    switch (state.value) {
        case '0':
            document.getElementById('dd1').style.display = "none" ;
            document.getElementById('dd2').style.display = "";
            state.value = "1";
            break;

        case '1':
            document.getElementById('dd1').style.display = "";
            document.getElementById('dd2').style.display = "none";
            state.value = "0";
            break;
    }
}